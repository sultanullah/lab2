Lab2.txt
===

## Code style and smells
What is good, bad, confusing about the code? Focus only on `Main.java` and `UBCPoet.java`.

Bad
-The text does not have well defined JAVADOC
-The methods are not easy to understand but then again we may not be required too
-Methods do not contain any specifications to help realize what the parameters are and what the return should be
-No rules given as to what type of words can be used to make poems

Good
-The names of the method makes it clear what should be expected when called i.e. what should be returned
-The main function is straightforward and understandable
-Method calls after leaving the main function are easy to follow through the debugger and they make sense
-Variable names are clear and relevant

## The First-Line Bug

The debugger mode in Eclipse was used to go through the program step by step and find the bug.
The first line bug was discovered in the code "hyp = pick(hypernyms(noun))" and from this it can be deduced that the method
called hypernyms creates this bug. The bug is reproducible and creates the string "metric linear unit" everytime the poem
is run depending on the word "song." In order to fix this bug one might erase the chance of it creating that string so an
if statement might catch the "metric linear unit" string and change it to something else. A slightly different algorithm
might be used to figure out the hypernym(method changed). 

## The Out-of-Bounds Bug

In order to reproduce the bug we must see exactly which method or assignment creates this bug and configure it in a way that
it keeps creating this bug. Through this, we can come up with a strategy to solve this bug since the bug is not reproducible. 

## The Illegal Argument Bug

The bug is in the library throwing the exception because a certain vocabulary of words is predetermined and only those words
will be taken by the program. Anything that does not appear to be in the program will be throwing an IllegalArgumentException.
Since the problem is that sense() sends an empty string one can trace back and use regular expressions to fix the bug.