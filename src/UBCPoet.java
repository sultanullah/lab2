import java.io.*;
import java.util.*;

import edu.mit.jwi.Dictionary;
import edu.mit.jwi.IDictionary;
import edu.mit.jwi.item.*;

/**
 * Simple poetry generator based on Flowerewolf.
 * 
 * See:
 * <a href="http://nodebox.net/code/index.php/Flowerewolf">Flowerewolf</a>,
 * <a href="http://wordnet.princeton.edu">Princeton WordNet</a>,
 * <a href="http://projects.csail.mit.edu/jwi/">MIT Java WordNet Interface</a>.
 * 
 * @author Sathish Gopalakrishnan
 */

//New class called UBCPoet
//Poetry generator
public class UBCPoet { 
    
	//vocab is a list of strings 
	//Can only be initialized once(final)
	//Can only be used within the class(private)
	private final List<String> vocab;  
    
	//dict is a variable of type IDictionary.
	//IDictionary is a generic collection of key/value pairs
	//Can only be intialized once
	private final IDictionary dict;
    
	//Constructor for UBCPoet class
	//Parameters are vocabulary and wordnet both of type File
	//File is an abstract representation of a file and directory pathnames
	//adds each line from the file Vocabulary onto the ArrayList vocab
	//Throws IOException 
	//dict is a new dictionary created from wordnet data
    public UBCPoet(File vocabulary, File wordnet) throws IOException {
        vocab = new ArrayList<String>();
        InputStream in = new FileInputStream(vocabulary);
        BufferedReader reader = new BufferedReader(new InputStreamReader(in));
        String line;
        while ((line = reader.readLine()) != null) {
            vocab.add(line);
        }
        reader.close();
        
        dict = new Dictionary(wordnet);
        dict.open();
    }
    
    //hyponyms is a method of the class UBCPoet
    //Parameters are a String called noun
    //returns the ArrayList result which is a list of hyponyms
    //"pigeon", "crow", "eagle" and "seagull" are all hyponyms of "bird" (their hypernym); which, in turn, is a hyponym of "animal".
    //"Subtype"
    public List<String> hyponyms(String noun) {
        IWordID id = dict.getIndexWord(noun, POS.NOUN).getWordIDs().get(0);
        ISynset synset = dict.getWord(id).getSynset();
        List<ISynsetID> hyponyms = synset.getRelatedSynsets(Pointer.HYPONYM);
        List<String> results = new ArrayList<String>();
        for (ISynsetID synsetId : hyponyms) {
            for (IWord word : dict.getSynset(synsetId).getWords()) {
                results.add(word.getLemma());
            }
        }
        return results;
    }

    //hypernyms is a method of the class UBCPoet
    //Parameters are a String called noun
    //returns an ArrayList which is a list of hypernyms
    //meaning includes the meanings of other words. For instance, flower is a hypernym of daisy and rose.
    //"Supertype"
    public List<String> hypernyms(String noun) {
        IWordID id = dict.getIndexWord(noun, POS.NOUN).getWordIDs().get(0);
        ISynset synset = dict.getWord(id).getSynset();
        List<ISynsetID> hyponyms = synset.getRelatedSynsets(Pointer.HYPERNYM);
        List<String> results = new ArrayList<String>();
        for (ISynsetID synsetId : hyponyms) {
            for (IWord word : dict.getSynset(synsetId).getWords()) {
                results.add(word.getLemma());
            }
        }
        return results;
    }

    //verse is a method of the class UBCPoet
    //Parameters are a String called noun
    //returns a String which is the verse of the poem
    
    public String verse(String noun) {
    	//id is of type IWordID which is a unique identifier sufficient to retrieve a particular word from the Wordnet database
    	//the value given to id is taken from dict.getIndexWord(noun, PO.NOUN).getWordIDs().get(0);
    	//dict is a field of the UBCPoet class of type IDictionary which is a generic collection of keys/value pairs
    	//getIndexWord(noun, POS.NOUN) is a method that takes in a lemma and part of speech (noun, POS.noun) and returns a index word corresponding to those two things(one index word!)
    	//getWordsID() is a method that returns an immutable(unchanging over time) list of word id objects that points to the words for this root form and part of speech combination
    	//get(0) gets index zero of the list of word id objects that is returned by getWordsID()(first word in the list)
    	
    	//get id from dictionary
        IWordID id = dict.getIndexWord(noun, POS.NOUN).getWordIDs().get(0); 
        
        //synset is of type ISynset which is a represents a synset
        //a sysnset is sets of synonyms(words with the same meaning)
        //dict is a field of the UBCPoet class of type IDictionary which is a generic collection of keys/value pairs
        //getWord(id)retrieves the word with the specificed id from the data list
        //id in this case is the first value from the list of words that are created from the root word which is the index word corresponding to the lemma and part of speech
        //getSynset() returns the synset(set of synonyms) uniquely identified by this word
        
        //get synset from dictionary
        ISynset synset = dict.getWord(id).getSynset();
        
        //new ArrayList called words
        List<String> words = new ArrayList<String>();
        
        //for(declaration : expression)
        //declaration is the variable available within the block and its value would be the same as the current array element
        //expression evaluates the array you need to loop through method call that returns an array
        for (String word : synset.getGloss().split("\\W+")) {
            if (sense(word, POS.NOUN)) {
                //uses the method eloquent within the class
            	words.add(eloquent(word));
            } else {
                words.add(word);
            }
        }
        StringBuilder result = new StringBuilder();
        for (int ii = 0; ii < words.size(); ii++) {
            result.append(words.get(ii));
            if (ii % 4 == 0) {
                result.append("\n");
            } else {
                result.append(" ");
            }
        }
        return result.toString().replaceAll("_", " ").trim();
    }

    
    //sense is a method of the class UBCPoet
    //Parameters are word(string) and pos(POS)
    //returns true if the word and part of speech is not null
    public boolean sense(String word, POS pos) {
        return dict.getIndexWord(word, pos) != null;
    }

    public String eloquent(String noun) {
        String hyp = pick(hyponyms(noun));
        if (hyp == null) {
            hyp = pick(hypernyms(noun));
        }
        if (hyp == null) {
            hyp = noun;
        }
        String adjective = alliterate(hyp, POS.NOUN);
        if (adjective == null) {
            return hyp;
        }
        return adjective + " " + hyp;
    }

    private String alliterate(String word, POS pos) {
        List<String> alliterations = alliterations(word);
        if (pos == POS.NOUN) {
            return pick(only(alliterations, POS.ADJECTIVE));
        }
        return pick(alliterations);
    }

    private List<String> only(List<String> words, POS pos) {
        List<String> result = new ArrayList<String>();
        for (String word : words) {
            if (sense(word, pos)) {
                result.add(word);
            }
        }
        return result;
    }

    private List<String> alliterations(String word) {
        String head = word.substring(0, 2);
        String foot = word.substring(word.length()-1);
        List<String> result = new ArrayList<String>();
        for (String other : vocab) {
            if (other.startsWith(head) && other.endsWith(foot)) {
                result.add(other);
            }
        }
        return result;
    }

    private <T> T pick(List<T> items) {
        if (items.isEmpty()) {
            return null;
        }
        return items.get((int)(Math.random() * items.size()));
    }
}